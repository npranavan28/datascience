import csv
import pymongo

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["datascience1"]
mycol = mydb["temperatures"]

with open('GlobalLandTemperaturesByCity.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=',')

    thislist = []

    for row in readCSV:
        #print(row)
        #print(row[0])
        thisdict = {
            "dt": row[0],
            "AverageTemperature": row[1],
            "AverageTemperatureUncertainty": row[2],
            "City": row[3],
            "Country": row[4]
            "Latitude": row[5]
            "Longitude": row[6]
        }
        thislist.append(thisdict)
        #print(row[0],row[1],row[2],)
        #print(thislist)
        mycol.insert_one(thisdict)